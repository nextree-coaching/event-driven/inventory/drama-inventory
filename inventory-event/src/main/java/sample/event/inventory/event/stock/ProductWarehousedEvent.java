package sample.event.inventory.event.stock;

import io.naradrama.prologue.domain.cqrs.event.CqrsDomainEvent;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
public class ProductWarehousedEvent extends CqrsDomainEvent {
    //
    private String productId;
    private int increasedQuantity;

    public ProductWarehousedEvent(String productId, int increasedQuantity) {
        //
        super();
        this.productId = productId;
        this.increasedQuantity = increasedQuantity;
    }

    public String toString() {
        //
        return toJson();
    }

    public static ProductWarehousedEvent sample() {
        //
        return new ProductWarehousedEvent(UUID.randomUUID().toString(), 10);
    }

    public static void main(String[] args) {
        //
        System.out.println(sample());
    }
}
