package sample.event.inventory.event.stock;

import io.naradrama.prologue.domain.cqrs.event.CqrsDomainEvent;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
public class ProductDeliveredEvent extends CqrsDomainEvent {
    //
    private String productId;
    private int decreasedQuantity;

    public ProductDeliveredEvent(String productId, int decreasedQuantity) {
        //
        super();
        this.productId = productId;
        this.decreasedQuantity = decreasedQuantity;
    }

    public String toString() {
        //
        return toJson();
    }

    public static ProductDeliveredEvent sample() {
        //
        return new ProductDeliveredEvent(UUID.randomUUID().toString(), 10);
    }

    public static void main(String[] args) {
        //
        System.out.println(sample());
    }
}
