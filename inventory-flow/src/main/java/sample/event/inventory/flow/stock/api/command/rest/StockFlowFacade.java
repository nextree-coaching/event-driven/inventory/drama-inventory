package sample.event.inventory.flow.stock.api.command.rest;

import io.naradrama.prologue.domain.cqrs.command.CommandResponse;
import sample.event.inventory.flow.stock.api.command.command.DeliverProductCommand;
import sample.event.inventory.flow.stock.api.command.command.WarehouseProductCommand;

public interface StockFlowFacade {
    //
    CommandResponse warehouseProduct(WarehouseProductCommand command);
    CommandResponse deliverProduct(DeliverProductCommand command);
}
