package sample.event.inventory.flow.stock.api.command.command;

import io.naradrama.prologue.domain.IdName;
import io.naradrama.prologue.domain.cqrs.TraceHeader;
import io.naradrama.prologue.domain.stage.ActorKey;

import java.util.UUID;

public class CommandSample {
    //
    public static void main(String[] args) {
        //
        TraceHeader traceHeader = new TraceHeader();
        traceHeader.setTraceId(UUID.randomUUID().toString());
        traceHeader.setLoginUser(IdName.sample());
        traceHeader.setUserId(ActorKey.sample().getId());

        WarehouseProductCommand warehouseProductCommandSample = WarehouseProductCommand.sample();
        warehouseProductCommandSample.setTraceHeader(traceHeader);
        System.out.println(warehouseProductCommandSample);

        DeliverProductCommand deliverProductCommandSample = DeliverProductCommand.sample();
        traceHeader.setTraceId(UUID.randomUUID().toString());
        deliverProductCommandSample.setTraceHeader(traceHeader);
        System.out.println(deliverProductCommandSample);
    }
}
