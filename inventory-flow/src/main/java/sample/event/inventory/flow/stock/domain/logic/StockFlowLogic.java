package sample.event.inventory.flow.stock.domain.logic;

import io.naradrama.prologue.domain.cqrs.command.CommandResponse;
import io.naraplatform.daysboy.EventStream;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import sample.event.inventory.aggregate.stock.domain.logic.StockUserLogic;
import sample.event.inventory.event.stock.ProductDeliveredEvent;
import sample.event.inventory.event.stock.ProductWarehousedEvent;
import sample.event.inventory.flow.stock.api.command.command.DeliverProductCommand;
import sample.event.inventory.flow.stock.api.command.command.WarehouseProductCommand;
import sample.event.inventory.flow.stock.api.command.rest.StockFlowFacade;

import javax.transaction.Transactional;

@Service
@Transactional
@RequiredArgsConstructor
public class StockFlowLogic implements StockFlowFacade {
    //
    private final StockUserLogic stockUserLogic;
    private final EventStream eventStream;

    @Override
    public CommandResponse warehouseProduct(WarehouseProductCommand command) {
        //
        String productId = command.getProductId();
        int quantity = command.getQuantity();
        stockUserLogic.increase(productId, quantity);
        eventStream.publishEvent(new ProductWarehousedEvent(productId, quantity));

        return new CommandResponse(productId);
    }

    @Override
    public CommandResponse deliverProduct(DeliverProductCommand command) {
        //
        String productId = command.getProductId();
        int quantity = command.getQuantity();
        stockUserLogic.decrease(productId, quantity);
        eventStream.publishEvent(new ProductDeliveredEvent(productId, quantity));

        return new CommandResponse(productId);
    }
}
