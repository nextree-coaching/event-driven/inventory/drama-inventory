package sample.event.inventory.flow.stock.api.command.rest;

import io.naradrama.prologue.domain.cqrs.command.CommandResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import sample.event.inventory.flow.stock.api.command.command.DeliverProductCommand;
import sample.event.inventory.flow.stock.api.command.command.WarehouseProductCommand;
import sample.event.inventory.flow.stock.domain.logic.StockFlowLogic;

@RestController
@RequestMapping("/flow/stock")
@RequiredArgsConstructor
public class StockFlowResource implements StockFlowFacade{
    //
    private final StockFlowLogic stockFlowLogic;

    @Override
    @PostMapping("/warehouse")
    public CommandResponse warehouseProduct(@RequestBody WarehouseProductCommand command) {
        //
        return stockFlowLogic.warehouseProduct(command);
    }

    @Override
    @PostMapping("/deliver")
    public CommandResponse deliverProduct(@RequestBody DeliverProductCommand command) {
        //
        return stockFlowLogic.deliverProduct(command);
    }
}
