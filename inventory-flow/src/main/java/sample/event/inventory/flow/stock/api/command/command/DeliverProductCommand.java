package sample.event.inventory.flow.stock.api.command.command;

import io.naradrama.prologue.domain.cqrs.command.CqrsUserCommand;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class DeliverProductCommand extends CqrsUserCommand {
    //
    private String productId;
    private int quantity;

    public String toString() {
        //
        return toJson();
    }

    public static DeliverProductCommand sample() {
        //
        return new DeliverProductCommand(UUID.randomUUID().toString(), 5);
    }
}
