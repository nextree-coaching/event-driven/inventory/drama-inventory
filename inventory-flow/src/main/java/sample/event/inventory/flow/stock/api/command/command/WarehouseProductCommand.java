package sample.event.inventory.flow.stock.api.command.command;

import io.naradrama.prologue.domain.cqrs.command.CqrsBaseCommandType;
import io.naradrama.prologue.domain.cqrs.command.CqrsUserCommand;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class WarehouseProductCommand extends CqrsUserCommand {
    //
    private String productId;
    private int quantity;

    public String toString() {
        //
        return toJson();
    }

    public static WarehouseProductCommand sample() {
        //
        WarehouseProductCommand sample = new WarehouseProductCommand(UUID.randomUUID().toString(), 10);

        return sample;
    }

    public static void main(String[] args) {
        //
        System.out.println(sample());
    }
}
