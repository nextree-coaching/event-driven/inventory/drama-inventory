package sample.event.inventory.flow.stock.domain.logic;

import io.naradrama.prologue.domain.IdName;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import sample.event.inventory.aggregate.stock.domain.entity.sdo.StockCdo;
import sample.event.inventory.aggregate.stock.domain.logic.StockLogic;
import sample.event.inventory.aggregate.stock.domain.logic.StockUserLogic;
import sample.event.order.event.order.OrderCanceledEvent;
import sample.event.order.event.order.ProductSoldEvent;
import sample.event.product.event.product.ProductDomainEvent;

@Slf4j
@Component
@RequiredArgsConstructor
public class StockHandlerLogic {
    //
    private final StockLogic stockLogic;
    private final StockUserLogic stockUserLogic;
    public void onProduct(ProductDomainEvent event) {
        //
        log.debug("onProductEvent : \n{}", event.toString());
        switch (event.getEventType()) {
            case Registered:
                newStock(event);
                break;
            case Modified:
                break;
            case Removed:
                break;
        }
    }

    private void newStock(ProductDomainEvent event) {
        //
        StockCdo cdo = new StockCdo(event.getActorKey(),
                IdName.of(event.getId(), event.getName()),
                0);

        stockLogic.registerStock(cdo);
        log.debug("Stock registered\n{}", cdo);
    }

    public void onSold(ProductSoldEvent event) {
        //
        stockUserLogic.decrease(event.getProductId(), event.getQuantity());
    }

    public void onOrderCanceled(OrderCanceledEvent event) {
        //
        stockUserLogic.increase(event.getProductId(), event.getQuantity());
    }
}
