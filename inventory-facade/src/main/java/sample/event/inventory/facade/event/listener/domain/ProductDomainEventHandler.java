package sample.event.inventory.facade.event.listener.domain;

import sample.event.inventory.flow.stock.domain.logic.StockHandlerLogic;
import sample.event.product.event.product.ProductDomainEvent;
import io.naraplatform.daysboy.event.DaysmanEventHandler;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;

@Slf4j
@Component
@RequiredArgsConstructor
@Transactional
public class ProductDomainEventHandler {
    //
    private final StockHandlerLogic stockHandlerLogic;

    @DaysmanEventHandler
    public void handle(ProductDomainEvent event) {
        //
        stockHandlerLogic.onProduct(event);
    }
}
