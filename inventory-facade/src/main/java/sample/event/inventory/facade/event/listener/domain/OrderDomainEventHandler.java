package sample.event.inventory.facade.event.listener.domain;

import io.naraplatform.daysboy.event.DaysmanEventHandler;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import sample.event.inventory.flow.stock.domain.logic.StockHandlerLogic;
import sample.event.order.event.order.OrderCanceledEvent;
import sample.event.order.event.order.ProductSoldEvent;

import javax.transaction.Transactional;

@Slf4j
@Component
@RequiredArgsConstructor
@Transactional
public class OrderDomainEventHandler {
    //
    private final StockHandlerLogic stockHandlerLogic;

    @DaysmanEventHandler
    public void handle(ProductSoldEvent event) {
        //
        stockHandlerLogic.onSold(event);
    }

    @DaysmanEventHandler
    public void handle(OrderCanceledEvent event) {
        //
        stockHandlerLogic.onOrderCanceled(event);
    }
}
