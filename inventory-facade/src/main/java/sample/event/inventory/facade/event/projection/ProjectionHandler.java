/*
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package sample.event.inventory.facade.event.projection;

import io.naradrama.prologue.domain.cqrs.broker.StreamEventMessage;
import sample.event.inventory.aggregate.stock.domain.event.StockEvent;
import sample.event.inventory.aggregate.stock.domain.logic.StockLogic;

public class ProjectionHandler {
    /* Autogen by nara studio */
    private final StockLogic stockLogic;

    public ProjectionHandler(StockLogic stockLogic) {
        /* Autogen by nara studio */
        this.stockLogic = stockLogic;
    }

    public void handle(StreamEventMessage streamEventMessage) {
        /* Autogen by nara studio */
        String classFullName = streamEventMessage.getPayloadClass();
        String payload = streamEventMessage.getPayload();
        String eventName = classFullName.substring(classFullName.lastIndexOf(".") + 1);
        switch(eventName) {
            case "StockEvent":
                StockEvent stockEvent = StockEvent.fromJson(payload);
                stockLogic.handleEventForProjection(stockEvent);
                break;
        }
    }
}
