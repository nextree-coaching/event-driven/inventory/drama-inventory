/*
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package sample.event.inventory.aggregate.stock.api.command.rest;

import sample.event.inventory.aggregate.stock.api.command.command.StockCommand;
import sample.event.inventory.aggregate.stock.domain.logic.StockLogic;
import io.naradrama.prologue.domain.cqrs.command.CommandResponse;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/aggregate/stock")
public class StockResource implements StockFacade {
    /* Autogen by nara studio */
    private final StockLogic stockLogic;

    public StockResource(StockLogic stockLogic) {
        /* Autogen by nara studio */
        this.stockLogic = stockLogic;
    }

    @Override
    @PostMapping("/stock/command")
    public CommandResponse executeStock(@RequestBody StockCommand stockCommand) {
        /* Autogen by nara studio */
        return stockLogic.routeCommand(stockCommand).getCommandResponse();
    }
}
