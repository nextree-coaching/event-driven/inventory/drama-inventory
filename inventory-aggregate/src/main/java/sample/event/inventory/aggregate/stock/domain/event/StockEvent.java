/*
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package sample.event.inventory.aggregate.stock.domain.event;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import io.naradrama.prologue.domain.cqrs.event.CqrsDataEvent;
import sample.event.inventory.aggregate.stock.domain.entity.Stock;
import io.naradrama.prologue.domain.NameValueList;
import io.naradrama.prologue.domain.cqrs.event.CqrsDataEventType;
import io.naradrama.prologue.util.json.JsonUtil;

@Getter
@Setter
@NoArgsConstructor
public class StockEvent extends CqrsDataEvent {
    /* Autogen by nara studio */
    private Stock stock;
    private String stockId;
    private NameValueList nameValues;

    protected StockEvent(CqrsDataEventType type) {
        /* Autogen by nara studio */
        super(type);
    }

    public static StockEvent newStockRegisteredEvent(Stock stock) {
        /* Autogen by nara studio */
        StockEvent event = new StockEvent(CqrsDataEventType.Registered);
        event.setStock(stock);
        return event;
    }

    public static StockEvent newStockModifiedEvent(String stockId, NameValueList nameValues) {
        /* Autogen by nara studio */
        StockEvent event = new StockEvent(CqrsDataEventType.Modified);
        event.setStockId(stockId);
        event.setNameValues(nameValues);
        return event;
    }

    public static StockEvent newStockRemovedEvent(String stockId) {
        /* Autogen by nara studio */
        StockEvent event = new StockEvent(CqrsDataEventType.Removed);
        event.setStockId(stockId);
        return event;
    }

    public static StockEvent newStockRemovedEvent(Stock stock) {
        /* Autogen by nara studio */
        StockEvent event = new StockEvent(CqrsDataEventType.Removed);
        event.setStock(stock);
        return event;
    }

    public String toString() {
        /* Autogen by nara studio */
        return toJson();
    }

    public static StockEvent fromJson(String json) {
        /* Autogen by nara studio */
        return JsonUtil.fromJson(json, StockEvent.class);
    }
}
