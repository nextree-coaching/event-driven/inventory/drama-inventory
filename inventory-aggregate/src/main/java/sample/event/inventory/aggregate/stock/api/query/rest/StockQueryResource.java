/*
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package sample.event.inventory.aggregate.stock.api.query.rest;

import sample.event.inventory.aggregate.stock.api.query.query.StockDynamicQuery;
import sample.event.inventory.aggregate.stock.api.query.query.StockQuery;
import sample.event.inventory.aggregate.stock.api.query.query.StocksDynamicQuery;
import sample.event.inventory.aggregate.stock.store.StockStore;
import sample.event.inventory.aggregate.stock.store.maria.jpo.StockJpo;
import io.naradrama.prologue.domain.cqrs.query.QueryResponse;
import io.naradrama.prologue.util.query.RdbQueryRequest;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.persistence.EntityManager;

@RestController
@RequestMapping("/aggregate/stock/stock/query")
public class StockQueryResource implements StockQueryFacade {
    //
    private final StockStore stockStore;
    private final RdbQueryRequest<StockJpo> request;

    public StockQueryResource(StockStore stockStore, EntityManager entityManager) {
        //
        this.stockStore = stockStore;
        this.request = new RdbQueryRequest<>(entityManager);
    }

    @Override
    @PostMapping("/")
    public QueryResponse execute(@RequestBody StockQuery stockQuery) {
        //
        stockQuery.execute(stockStore);
        return stockQuery.getQueryResponse();
    }

    @Override
    @PostMapping("/dynamic-single")
    public QueryResponse execute(@RequestBody StockDynamicQuery stockDynamicQuery) {
        //
        stockDynamicQuery.execute(request);
        return stockDynamicQuery.getQueryResponse();
    }

    @Override
    @PostMapping("/dynamic-multi")
    public QueryResponse execute(@RequestBody StocksDynamicQuery stocksDynamicQuery) {
        //
        stocksDynamicQuery.execute(request);
        return stocksDynamicQuery.getQueryResponse();
    }
}
