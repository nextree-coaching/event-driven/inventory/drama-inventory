/*
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package sample.event.inventory.aggregate.stock.api.command.command;

import lombok.Getter;
import lombok.Setter;
import lombok.NoArgsConstructor;
import io.naradrama.prologue.domain.cqrs.command.CqrsBaseCommand;
import sample.event.inventory.aggregate.stock.domain.entity.sdo.StockCdo;
import java.util.List;
import io.naradrama.prologue.domain.NameValueList;
import io.naradrama.prologue.domain.cqrs.command.CqrsBaseCommandType;
import io.naradrama.prologue.util.json.JsonUtil;

@Getter
@Setter
@NoArgsConstructor
public class StockCommand extends CqrsBaseCommand {
    //
    private StockCdo stockCdo;
    private List<StockCdo> stockCdos;
    private boolean multiCdo;
    private String stockId;
    private NameValueList nameValues;

    protected StockCommand(CqrsBaseCommandType type) {
        //
        super(type);
    }

    public static StockCommand newRegisterStockCommand(StockCdo stockCdo) {
        //
        StockCommand command = new StockCommand(CqrsBaseCommandType.Register);
        command.setStockCdo(stockCdo);
        return command;
    }

    public static StockCommand newRegisterStockCommand(List<StockCdo> stockCdos) {
        //
        StockCommand command = new StockCommand(CqrsBaseCommandType.Register);
        command.setStockCdos(stockCdos);
        command.setMultiCdo(true);
        return command;
    }

    public static StockCommand newModifyStockCommand(String stockId, NameValueList nameValues) {
        //
        StockCommand command = new StockCommand(CqrsBaseCommandType.Modify);
        command.setStockId(stockId);
        command.setNameValues(nameValues);
        return command;
    }

    public static StockCommand newRemoveStockCommand(String stockId) {
        //
        StockCommand command = new StockCommand(CqrsBaseCommandType.Remove);
        command.setStockId(stockId);
        return command;
    }

    public String toString() {
        //
        return toJson();
    }

    public static StockCommand fromJson(String json) {
        //
        return JsonUtil.fromJson(json, StockCommand.class);
    }

    public static StockCommand sampleForRegister() {
        //
        return newRegisterStockCommand(StockCdo.sample());
    }

    public static void main(String[] args) {
        //
        System.out.println(sampleForRegister());
    }
}
