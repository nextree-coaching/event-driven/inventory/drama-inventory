/*
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package sample.event.inventory.aggregate.stock.domain.logic;

import io.naraplatform.daysboy.EventStream;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import sample.event.inventory.aggregate.stock.store.StockStore;
import sample.event.inventory.aggregate.stock.api.command.command.StockCommand;
import io.naradrama.prologue.domain.cqrs.command.CommandResponse;
import io.naradrama.prologue.domain.cqrs.FailureMessage;
import java.util.Optional;
import sample.event.inventory.aggregate.stock.domain.entity.sdo.StockCdo;
import sample.event.inventory.aggregate.stock.domain.event.StockEvent;
import io.naradrama.prologue.domain.NameValueList;
import java.util.List;
import java.util.stream.Collectors;
import sample.event.inventory.aggregate.stock.domain.entity.Stock;

import java.util.NoSuchElementException;

@Service
@Transactional
public class StockLogic {
    //
    private final StockStore stockStore;
    private final EventStream eventStreamService;

    public StockLogic(StockStore stockStore, EventStream eventStreamService) {
        //
        this.stockStore = stockStore;
        this.eventStreamService = eventStreamService;
    }

    public StockCommand routeCommand(StockCommand command) {
        //
        switch(command.getCqrsBaseCommandType()) {
            case Register:
                if (command.isMultiCdo()) {
                    List<String> entityIds = this.registerStocks(command.getStockCdos());
                    command.setCommandResponse(new CommandResponse(entityIds));
                } else {
                    String entityId = Optional.ofNullable(command.getNameValues()).filter(nameValueList -> command.getNameValues().size() != 0).map(nameValueList -> this.registerStock(command.getStockCdo(), nameValueList)).orElse(this.registerStock(command.getStockCdo()));
                    command.setCommandResponse(new CommandResponse(entityId));
                }
                break;
            case Modify:
                this.modifyStock(command.getStockId(), command.getNameValues());
                command.setCommandResponse(new CommandResponse(command.getStockId()));
                break;
            case Remove:
                this.removeStock(command.getStockId());
                command.setCommandResponse(new CommandResponse(command.getStockId()));
                break;
            default:
                command.setFailureMessage(new FailureMessage(new Throwable("CommandType must be Register, Modify or Remove")));
        }
        return command;
    }

    public String registerStock(StockCdo stockCdo) {
        //
        Stock stock = new Stock(stockCdo);
        if (stockStore.exists(stock.getEntityId())) {
            throw new IllegalArgumentException("stock already exists. " + stock.getEntityId());
        }
        stockStore.create(stock);
        StockEvent stockEvent = StockEvent.newStockRegisteredEvent(stock);
        eventStreamService.publishEvent(stockEvent);
        return stock.getEntityId();
    }

    public String registerStock(StockCdo stockCdo, NameValueList nameValueList) {
        //
        Stock stock = Stock.newInstance(stockCdo, nameValueList);
        if (stockStore.exists(stock.getEntityId())) {
            throw new IllegalArgumentException("stock already exists. " + stock.getEntityId());
        }
        stockStore.create(stock);
        StockEvent stockEvent = StockEvent.newStockRegisteredEvent(stock);
        eventStreamService.publishEvent(stockEvent);
        return stock.getEntityId();
    }

    public List<String> registerStocks(List<StockCdo> stockCdos) {
        //
        return stockCdos.stream().map(stockCdo -> this.registerStock(stockCdo)).collect(Collectors.toList());
    }

    public Stock findStock(String stockId) {
        //
        Stock stock = stockStore.retrieve(stockId);
        if (stock == null) {
            throw new NoSuchElementException("Stock id: " + stockId);
        }
        return stock;
    }

    public void modifyStock(String stockId, NameValueList nameValues) {
        //
        Stock stock = findStock(stockId);
        stock.modify(nameValues);
        stockStore.update(stock);
        StockEvent stockEvent = StockEvent.newStockModifiedEvent(stockId, nameValues);
        eventStreamService.publishEvent(stockEvent);
    }

    public void removeStock(String stockId) {
        //
        Stock stock = findStock(stockId);
        stockStore.delete(stock);
        StockEvent stockEvent = StockEvent.newStockRemovedEvent(stock.getEntityId());
        eventStreamService.publishEvent(stockEvent);
    }

    public boolean existsStock(String stockId) {
        //
        return stockStore.exists(stockId);
    }

    public void handleEventForProjection(StockEvent stockEvent) {
        //
        switch(stockEvent.getCqrsDataEventType()) {
            case Registered:
                stockStore.create(stockEvent.getStock());
                break;
            case Modified:
                Stock stock = stockStore.retrieve(stockEvent.getStockId());
                stock.modify(stockEvent.getNameValues());
                stockStore.update(stock);
                break;
            case Removed:
                stockStore.delete(stockEvent.getStockId());
                break;
        }
    }
}
