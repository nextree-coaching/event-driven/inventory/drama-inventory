/*
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package sample.event.inventory.aggregate.stock.store.maria.jpo;

import lombok.Getter;
import lombok.Setter;
import lombok.NoArgsConstructor;
import javax.persistence.Entity;
import javax.persistence.Table;
import io.naradrama.prologue.store.jpa.StageEntityJpo;
import sample.event.inventory.aggregate.stock.domain.entity.Stock;
import org.springframework.beans.BeanUtils;

import java.util.List;
import java.util.stream.Collectors;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "STOCK")
public class StockJpo extends StageEntityJpo {
    //
    private String productName;
    private int quantity;

    public StockJpo(Stock stock) {
        //
        super(stock);
        BeanUtils.copyProperties(stock, this);
    }

    public Stock toDomain() {
        //
        Stock stock = new Stock(getEntityId(), genActorKey());
        BeanUtils.copyProperties(this, stock);
        return stock;
    }

    public static List<Stock> toDomains(List<StockJpo> stockJpos) {
        //
        return stockJpos.stream().map(StockJpo::toDomain).collect(Collectors.toList());
    }

    public String toString() {
        //
        return toJson();
    }

    public static StockJpo sample() {
        //
        return new StockJpo(Stock.sample());
    }

    public static void main(String[] args) {
        //
        System.out.println(sample());
    }
}
