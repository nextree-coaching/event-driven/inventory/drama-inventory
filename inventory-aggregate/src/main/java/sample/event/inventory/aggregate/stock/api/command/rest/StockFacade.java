/*
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package sample.event.inventory.aggregate.stock.api.command.rest;

import io.naradrama.prologue.domain.cqrs.command.CommandResponse;
import sample.event.inventory.aggregate.stock.api.command.command.StockCommand;

public interface StockFacade {
    /* Autogen by nara studio */
    CommandResponse executeStock(StockCommand stockCommand);
}
