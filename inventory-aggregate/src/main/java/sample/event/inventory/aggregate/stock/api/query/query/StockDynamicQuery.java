/*
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package sample.event.inventory.aggregate.stock.api.query.query;

import lombok.Getter;
import lombok.Setter;
import lombok.NoArgsConstructor;
import io.naradrama.prologue.domain.cqrs.query.CqrsDynamicQuery;
import sample.event.inventory.aggregate.stock.domain.entity.Stock;
import io.naradrama.prologue.util.query.RdbQueryRequest;
import sample.event.inventory.aggregate.stock.store.maria.jpo.StockJpo;
import io.naradrama.prologue.util.query.RdbQueryBuilder;

import javax.persistence.TypedQuery;
import java.util.Optional;

@Getter
@Setter
@NoArgsConstructor
public class StockDynamicQuery extends CqrsDynamicQuery<Stock> {
    /* Autogen by nara studio */

    public void execute(RdbQueryRequest<StockJpo> request) {
        /* Autogen by nara studio */
        request.addQueryStringAndClass(genSqlString(), StockJpo.class);
        TypedQuery<StockJpo> query = RdbQueryBuilder.build(request);
        StockJpo stockJpo = query.getSingleResult();
        setQueryResult(Optional.ofNullable(stockJpo).map(jpo -> jpo.toDomain()).orElse(null));
    }
}
