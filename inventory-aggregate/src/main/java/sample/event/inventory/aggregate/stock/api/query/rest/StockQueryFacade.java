/*
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package sample.event.inventory.aggregate.stock.api.query.rest;

import io.naradrama.prologue.domain.cqrs.query.QueryResponse;
import sample.event.inventory.aggregate.stock.api.query.query.StockQuery;
import sample.event.inventory.aggregate.stock.api.query.query.StockDynamicQuery;
import sample.event.inventory.aggregate.stock.api.query.query.StocksDynamicQuery;

public interface StockQueryFacade {
    /* Autogen by nara studio */
    QueryResponse execute(StockQuery stockQuery);
    QueryResponse execute(StockDynamicQuery stockDynamicQuery);
    QueryResponse execute(StocksDynamicQuery stocksDynamicQuery);
}
