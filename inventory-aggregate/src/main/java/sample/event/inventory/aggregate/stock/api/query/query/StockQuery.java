/*
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package sample.event.inventory.aggregate.stock.api.query.query;

import lombok.Getter;
import lombok.Setter;
import lombok.NoArgsConstructor;
import io.naradrama.prologue.domain.cqrs.query.CqrsBaseQuery;
import sample.event.inventory.aggregate.stock.domain.entity.Stock;
import sample.event.inventory.aggregate.stock.store.StockStore;

@Getter
@Setter
@NoArgsConstructor
public class StockQuery extends CqrsBaseQuery<Stock> {
    /* Autogen by nara studio */
    private String stockId;

    public void execute(StockStore stockStore) {
        /* Autogen by nara studio */
        setQueryResult(stockStore.retrieve(stockId));
    }
}
