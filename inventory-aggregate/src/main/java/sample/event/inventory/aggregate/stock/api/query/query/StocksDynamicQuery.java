/*
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package sample.event.inventory.aggregate.stock.api.query.query;

import lombok.Getter;
import lombok.Setter;
import lombok.NoArgsConstructor;
import io.naradrama.prologue.domain.cqrs.query.CqrsDynamicQuery;
import java.util.List;
import sample.event.inventory.aggregate.stock.domain.entity.Stock;
import io.naradrama.prologue.util.query.RdbQueryRequest;
import sample.event.inventory.aggregate.stock.store.maria.jpo.StockJpo;
import io.naradrama.prologue.util.query.RdbQueryBuilder;
import javax.persistence.TypedQuery;
import java.util.ArrayList;
import io.naradrama.prologue.domain.Offset;

import java.util.Optional;
import static java.util.Objects.nonNull;

@Getter
@Setter
@NoArgsConstructor
public class StocksDynamicQuery extends CqrsDynamicQuery<List<Stock>> {
    /* Autogen by nara studio */

    public void execute(RdbQueryRequest<StockJpo> request) {
        /* Autogen by nara studio */
        request.addQueryStringAndClass(genSqlString(), StockJpo.class);
        Offset offset = getOffset();
        TypedQuery<StockJpo> query = RdbQueryBuilder.build(request, offset);
        query.setFirstResult(offset.getOffset());
        query.setMaxResults(offset.getLimit());
        List<StockJpo> stockJpos = query.getResultList();
        setQueryResult(Optional.ofNullable(stockJpos).map(jpos -> StockJpo.toDomains(jpos)).orElse(new ArrayList<>()));
        if (nonNull(getOffset()) && getOffset().isTotalCountRequested()) {
            TypedQuery<Long> countQuery = RdbQueryBuilder.buildForCount(request);
            long totalCount = countQuery.getSingleResult();
            Offset countableOffset = getOffset();
            countableOffset.setTotalCount((int) totalCount);
            setOffset(countableOffset);
        }
    }
}
