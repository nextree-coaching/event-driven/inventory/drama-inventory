/*
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package sample.event.inventory.aggregate.stock.store.maria.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import sample.event.inventory.aggregate.stock.store.maria.jpo.StockJpo;

public interface StockMariaRepository extends PagingAndSortingRepository<StockJpo, String> {
    /* Autogen by nara studio */
}
