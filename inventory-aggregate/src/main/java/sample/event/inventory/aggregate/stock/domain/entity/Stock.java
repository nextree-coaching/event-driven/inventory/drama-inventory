package sample.event.inventory.aggregate.stock.domain.entity;

import sample.event.inventory.aggregate.stock.domain.entity.sdo.StockCdo;
import io.naradrama.prologue.domain.NameValue;
import io.naradrama.prologue.domain.NameValueList;
import io.naradrama.prologue.domain.ddd.DomainAggregate;
import io.naradrama.prologue.domain.ddd.Updatable;
import io.naradrama.prologue.domain.drama.StageEntity;
import io.naradrama.prologue.domain.stage.ActorKey;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class Stock extends StageEntity implements DomainAggregate {
    //
    private String productName;
    @Updatable
    private int quantity;

    public Stock(String id, ActorKey actorKey) {
        //
        super(id, actorKey);
    }

    public Stock(StockCdo cdo) {
        //
        super(cdo.getProduct().getId(), cdo.getActorKey());
        this.productName = cdo.getProduct().getName();
        this.quantity = cdo.getQuantity();
    }

    public static Stock newInstance(StockCdo cdo, NameValueList nameValue) {
        //
        Stock stock = new Stock(cdo);
        stock.modifyAttributes(nameValue);

        return stock;
    }

    @Override
    protected void modifyAttributes(NameValueList nameValueList) {
        //
        for(NameValue nameValue : nameValueList.list()) {
            String value = nameValue.getValue();
            switch (nameValue.getName()) {
                case "stock" :
                    quantity = Integer.parseInt(value);
                    break;
//                default:
//                    throw new IllegalAccessException(String.format("Attribute %s of Stock is not updateable", nameValue.getName()));
            }
        }
    }

    @Override
    public String toJson() {
        return super.toJson();
    }

    public static Stock sample() {
        //
        return new Stock(StockCdo.sample());
    }

    public static void main(String[] args) {
        //
        System.out.println(sample());
    }

//    public String getEntityId() {
//        //
//        return getId();
//    }
}
