package sample.event.inventory.aggregate.stock.domain.entity.sdo;

import io.naradrama.prologue.domain.IdName;
import io.naradrama.prologue.domain.stage.ActorKey;
import io.naradrama.prologue.util.json.JsonSerializable;
import io.naradrama.prologue.util.json.JsonUtil;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class StockCdo implements JsonSerializable {
    //
    private ActorKey actorKey;
    private IdName product;
    private int quantity;

    public String toString() {
        //
        return toJson();
    }

    public static StockCdo fromJson(String json) {
        //
        return JsonUtil.fromJson(json, StockCdo.class);
    }

    public static StockCdo sample() {
        //
        return new StockCdo(ActorKey.sample(),
                IdName.of(UUID.randomUUID().toString(), "dokdo belongs to korea"),
                0);
    }

    public static void main(String[] args) {
        //
        System.out.println(sample());
    }
}
